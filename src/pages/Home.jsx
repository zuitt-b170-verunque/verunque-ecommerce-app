import React, { useState, useEffect } from "react";

// BOOTSTRAP
import Container from 'react-bootstrap/Container'

// Page Component
import Product from '../pages/Products.jsx'

export default function Home({handleClick}){
    
    return(
        <Container fluid>
            <Product handleClick={handleClick}/>
        </Container>
    )
}