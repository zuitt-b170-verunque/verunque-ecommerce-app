const productModel =  [
    {
        id: "id001",
        name: "product 001",
        description: "Description 001",
        price: 100,
        amount: 1,
        isActive: true
    },
    {
        id: "id002",
        name: "product 002",
        description: "Description 002",
        price: 200,
        amount: 1,
        isActive: true
    },
    {
        id: "id003",
        name: "product 003",
        description: "Description 003",
        price: 300,
        amount: 1,
        isActive: true
    },
    {
        id: "id001",
        name: "product 001",
        description: "Description 001",
        price: 100,
        amount: 1,
        isActive: true
    },
    {
        id: "id002",
        name: "product 002",
        description: "Description 002",
        price: 200,
        amount: 1,
        isActive: true
    },
    {
        id: "id003",
        name: "product 003",
        description: "Description 003",
        price: 300,
        amount: 1,
        isActive: true
    }
]

export default productModel;