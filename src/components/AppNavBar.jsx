// Base Import
import React, {Fragment, useContext}  from 'react';
import {Link, NavLink, useNavigate} from 'react-router-dom';

// BOOTSTRAP

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Badge from 'react-bootstrap/Badge'
import Form from 'react-bootstrap/Form'

// App imports
import userContext from '../userContext.jsx';


// HTML Area
export default function AppNavBar({size}) {
    /*  */
    const {user} = useContext(userContext)

    return (

        <Navbar sticky="top" bg="primary" variant='dark' expand="lg">

            {/* LOGO */}
          <Navbar.Brand as={Link} to="/" href="#banner">Online Tindahan</Navbar.Brand>

          {/* Hamburger */}
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">

            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
            </Nav>

            <Nav className="ml-auto mr-5">
                {/* change to drop down */}

                { (user.isAdmin === true) ? 
               '' 
               :
               <Fragment>
                 <Nav.Link className="mr-2" as={Link} to="/cart"><i className="fas fa-cart-plus"></i><Badge bg="danger">{size}</Badge></Nav.Link>
               </Fragment>
              }

              { (user.id !== null) ? 
                  <NavDropdown title="Menu" id="basic-nav-dropdown" className="mr-5">
                    <NavDropdown.Item  as={Link} to="/logout">Logout</NavDropdown.Item>

                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} to="/history">History</NavDropdown.Item>
                </NavDropdown>
               :
               <Fragment>
                 <Nav.Link as={Link} to="/register">Register</Nav.Link>
                 <Nav.Link as={Link} to="/login">Log In</Nav.Link>
               </Fragment>
            }
            </Nav>
          </Navbar.Collapse>

      </Navbar>
      
      
      )
}