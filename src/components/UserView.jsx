import { Fragment, useState, useEffect } from 'react'
import ProductCard from './ProductCard'

import  productModel from'../models/ProductModel.js'

// App Component
import Banner from '../components/Banner.jsx'

export default function UserView({productModel, handleClick}) {

    
    
    const [product, setProduct] = useState([]);
    useEffect(() => {

        const arr = productModel.map(product => {
        	if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id} handleClick={handleClick}/>
				)
        	}else{
        		return null;
        	}
        });

        setProduct(arr);

    }, [productModel]);

    return(
        <Fragment>
            <Banner/>
            {product}
        </Fragment>
    );
}
