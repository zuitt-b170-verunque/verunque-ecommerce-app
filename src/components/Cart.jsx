import React, { useState, useEffect } from "react";
import {Navigate,useNavigate, Link} from 'react-router-dom';
import "../styles/cart.css";

import Swal from 'sweetalert2';

const Cart = ({ cart, setCart, handleChange }) => {
  
  const history = useNavigate();
  const [price, setPrice] = useState(0);

  const buy = (productId) => {

		fetch(`https://stormy-sierra-90274.herokuapp.com/api/orders/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				totalAmount: price,
        isPaid: true

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {
        
        Swal.fire({
          title: "Thank you for your purchase",
					icon: 'success',
					text: "View products to buy more."
				});
        history.push("/");
        
			} else {
        
        Swal.fire({
          title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});

    // <Navigate to='/' />
    // handleRemove()
	};
  const removeFromCart = (productToRemove) => {
    setCart(
      cart.filter((productProp) => productProp !== productToRemove)
    );
  };

  const handleRemove = (id) => {
    const arr = cart.filter((productProp) => productProp.id !== id);
    setCart(arr);
    handlePrice();
  };

  const handlePrice = () => {
    let ans = 0;
    cart.map((productProp) => (ans += productProp.amount * productProp.price));
    setPrice(ans);
  };

  useEffect(() => {
    handlePrice();
  });

  
  return (
    <article>
      {cart.map((productProp) => (
        
        <div className="cart_box" key={productProp.id}>
          <div>
            <p>{productProp.name}</p>
          </div>
          <div>
            <button onClick={() => handleChange(productProp, 1)}>+</button>
            <button>{productProp.amount}</button>
            <button onClick={() => handleChange(productProp, -1)}>-</button>
            <span>Price: </span>
            <span>Php {productProp.price.toFixed(2)}</span>
          </div>
        
          <div>
            
            <span>Php {productProp.price * productProp.amount}.00</span>
            <button onClick={() => handleRemove(productProp.id)}>Clear Cart</button>
            <button onClick={() => removeFromCart(productProp)}>
              Remove
            </button>
          </div>
        </div>
      ))}
      <div className="total">
        <span>Total Price of your Cart </span>
        <span>Php {price.toFixed(2)}</span>
        <hr/>

        {(price === 0) ?
          <button variant="outline-warning" onClick={() => buy( price)} disabled>
            Checkout
          </button> :
          <button variant="warning"  onClick={() => buy( price)}>
          Checkout
        </button>
        }

      </div>
    </article>
  );
};

export default Cart;
