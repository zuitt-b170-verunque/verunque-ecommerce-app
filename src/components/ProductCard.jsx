// base import
import React, {useContext} from 'react';
import { Link } from 'react-router-dom';

// BOOTSTRAP component
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import userContext from "../userContext.jsx"

export default function ProductCard({productProp, handleClick}){
    
    const {_id, name, description, price} = productProp;

    const {user} = useContext(userContext)
    // let product = props.product
    return(

                <Card style={{ width: '20rem' }}>
                    <Card.Img variant="top" src="https://dummyimage.com/200x200/293462/F1D00A" />
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php {price}</Card.Text>
                        { user.id !== null ? 
									<Link className="btn btn-warning btn-block" to="/cart">
									 <Button variant="outline-dark" onClick={() => {handleClick(productProp)}}>Add to Cart</Button>
                                    </Link>  
								: 
									<Link className="btn btn-warning btn-block" to="/login">Log in to Buy</Link>
							}
                        
                        <Link className="btn btn-primary btn-block" to={`/products/${_id}`}>Details</Link>

                    </Card.Body>
                </Card> 
    )

}
