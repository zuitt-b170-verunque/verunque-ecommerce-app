import React from 'react';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Figure from 'react-bootstrap/Figure';
import Image from 'react-bootstrap/Image'

import BannerImg from '../img/banner.png'

import '../styles/Banner.css';

export default function Banner () {
    return (
        <Row id="banner">
            
            <div id='banner-image'>
                <img src={BannerImg} />
            </div>

            <div id='banner-text'>
                <h1>Online Tindahan</h1>
                <p>Buy anything, anywhere</p>
            </div>
            
        </Row>
    )
}