// Base imports
import React, {useState, useEffect} from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// BOOTSTRAP
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

// App Components
import AppNavbar from './components/AppNavBar.jsx';

import Cart from './components/Cart.jsx';


// App Imports
import userContext from './userContext.jsx';

// PAge Components
import Home from './pages/Home.jsx'
import Login from './pages/Login.jsx'
import Register from './pages/Register.jsx'
import Logout from './pages/Logout.jsx'
import Products from './pages/Products.jsx'
import ProductDetails from './pages/ProductDetails.jsx'
import PurchaseHistory from './pages/PurchaseHistory.jsx'


const cartFromLocalStorage = JSON.parse(localStorage.getItem('cart') || '[]')

export default function App() {

    const [user, setUser] = useState({email: localStorage.getItem('access')});

    // clears the local sotrage and clears the item
    const unsetUser = () => {
        localStorage.clear();
        setUser({access:null})
    }

    // cart data
    const [cart, setCart] =useState(cartFromLocalStorage);
    const handleClick = (productProp) => {
        if (cart.indexOf(productProp) !== -1) return;
        setCart([...cart, productProp]);
        console.log(cart)
    }

    // save the cart data inside localStorage
    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [cart])


    const handleChange = (productProp, d) => {
        const ind = cart.indexOf(productProp);
        const arr = cart;
        arr[ind].amount += d;
    
        if (arr[ind].amount === 0) arr[ind].amount = 1;
        setCart([...arr]);
      };



    return (
        <userContext.Provider value={{user, setUser, unsetUser}}>
            <Router>
                    <AppNavbar user={user} size={cart.length} />    
                    <Routes>
                        <Route path ="/" element={<Home handleClick={handleClick}/>}/>
                        <Route path ="/products" element={<Products handleClick={handleClick} />}/>


                        <Route path ="/products/:productId" element={<ProductDetails/>}/>


                        <Route path ="/login" element={<Login/>}/>
                        <Route path ="/logout" element={<Logout/>}/>
                        <Route path ="/history" element={<PurchaseHistory/>}/>
                        <Route path ="/register" element={<Register/>}/>
                        <Route path ="/cart" element={<Cart cart={cart} setCart={setCart} handleChange={handleChange}/>}/>
                    </Routes>
            </Router>
        </userContext.Provider>
    )
}
